/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_00.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/03 23:47:30 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/04 00:27:15 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strdup(char *src);

int	main(void)
{
	char *a = "Hola Mundo.";
	char *b = ft_strdup(a);
	char *c = ft_strdup("");

	printf("a: \"%s\"\n", a);
	printf("b: \"%s\"\n", b);
	printf("c: \"%s\"\n", c);

	return (0);
}
