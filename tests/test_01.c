/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_01.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/04 00:27:36 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/04 01:13:00 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	*ft_range(int min, int max);

/**
 * @brief	Muestra los elementos de un array de enteros.
 * 			- Si el array es NULL, mostrará "NULL".
 *			- Solo muestra los números que haya en el array.
 * 
 * @param array 	Array de enteros.
 * @param size 		Tamaño del array.
 */
void	show_array(int *array, int size)
{
	int	i;

	if (array == NULL)
		printf("NULL");
	i = 0;
	while (i < size)
	{
		printf("%d", array[i]);
		if (i < size - 1)
			printf(", ");
		i++;
	}
	printf("\n");
}

int	main(void)
{
	int *a = ft_range(0, 10);
	int *b = ft_range(10, 0);
	int *c = ft_range(-10, 10);
	int *d = ft_range(10, 10);
	int *e = ft_range(-10, -10);

	printf("(0, 10):\t");	show_array(a, 11);
	printf("(10, 0):\t");	show_array(b, 0);
	printf("(-10, 10):\t");	show_array(c, 21);
	printf("(10, 10):\t");	show_array(d, 0);
	printf("(-10, -10):\t");	show_array(e, 0);
	
	return (0);
}
