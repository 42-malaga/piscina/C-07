#include <stdio.h>

char *ft_strjoin(int size, char **strs, char *sep);

int main(void)
{
    char *strs1[] = {};
    char *strs2[] = {"Hola", "Mundo: ", "bienvenido."};
    char *sep1 = " -> ";
    char *sep2 = "";

    printf("strs1: %s\n", ft_strjoin(0, strs1, sep1));
    printf("strs2: %s\n", ft_strjoin(3, strs2, sep1));
    printf("strs2: %s\n", ft_strjoin(3, strs2, sep2));

    return (0);
}
