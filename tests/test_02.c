#include <stdio.h>

int ft_ultimate_range(int **range, int min, int max);

/**
 * @brief	Muestra los elementos de un array de enteros.
 * 			- Si el array es NULL, mostrará "NULL".
 *			- Solo muestra los números que haya en el array.
 * 
 * @param array 	Array de enteros.
 * @param size 		Tamaño del array.
 */
void	show_array(int *array, int size)
{
	int	i;

	if (array == NULL)
		printf("NULL");
	i = 0;
	while (i < size)
	{
		printf("%d", array[i]);
		if (i < size - 1)
			printf(", ");
		i++;
	}
	printf("\n");
}

int main(void)
{
    int *a;
    int *b;
    int *c;
    int *d;

    int ra = ft_ultimate_range(&a, 0, 10);
    int rb = ft_ultimate_range(&b, 10, 0);
    int rc = ft_ultimate_range(&c, -10, 10);
    int rd = ft_ultimate_range(&d, 10, 10);

    printf("(0, 10):\t%d\t", ra);	show_array(a, ra);
    printf("(10, 0):\t%d\t", rb);	show_array(b, rb);
    printf("(-10, 10):\t%d\t", rc);	show_array(c, rc);
    printf("(10, 10):\t%d\t", rd);	show_array(d, rd);

    return (0);
}
