/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/07 01:48:31 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/10 01:38:27 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*act;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (i < size)
		j += ft_strlen(strs[i++]);
	act = (char *) malloc((j + (size - 1) * ft_strlen(sep) + 1) * sizeof(char));
	if (act == NULL)
		return (NULL);
	i = -1;
	while (++i < size)
	{
		j = 0;
		while (strs[i][j])
			act[k++] = strs[i][j++];
		j = 0;
		while (sep[j] && i < size - 1)
			act[k++] = sep[j++];
	}
	act[k] = '\0';
	return (act);
}
